import axios from 'axios';

const api = axios.create({
  baseURL: "https://64dfc52371c3335b25830a34.mockapi.io/"
});

export default api;
