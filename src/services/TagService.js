import api from './api';
import { toast } from 'react-toastify';


const tagFetch = async () => {
  try {
    const response = await api.get('/tags');
    return response.data;
  } catch (error) {
    console.error('Error fetching tags:', error);
    toast.error('Error Fetching Tags!');
    throw error;
  }
};

const tagAdd = async (tagName) => {
  try {
    const response = await api.post('/tags', { name: tagName });
    toast.success('Tag Added successfully.');
    return response.data;
  } catch (error) {
    console.error('Error adding Tag:', error);
    toast.error('Error Adding Tag!');
    throw error;
  }
};

const tagEdit = async (tagId, tagName) => {
  try {
    const response = await api.put(`/tags/${tagId}`, { name: tagName });
    toast.success('Tag Edited successfully.');
    return response.data;
  } catch (error) {
    console.error('Error editing tag:', error);
    toast.error('Error Editing Tag!');
    throw error;
  }
};

const Tagdelete = async (tagId) => {
  try {
    const response = await api.delete(`/tags/${tagId}`);
    toast.success('Tag Deleted successfully.');
    return response.data;
  } catch (error) {
    console.error('Error deleting tag:', error);
    toast.error('Error Deleting Tag!');
    throw error;
  }
};

export { tagFetch, tagAdd, tagEdit, Tagdelete };
