import { render, screen, fireEvent } from '@testing-library/react';
import TagContainer from './index';
import Tag from '@components/Tag';
import mockTagFetch from '@mocks/mockTagFetch';
import mockTagPost from '@mocks/mockTagPost';
import mockTagDelete from '@mocks/mockTagDelete';
import mockTagEdit from '@mocks/mockTagEdit';

describe('TagContainer Tests', () => {
  beforeEach(() => {
    jest.spyOn(window, 'fetch').mockImplementation((url, options) => {
      if (options.method === 'POST') {
        return mockTagPost(url);
      } else if (options.method === 'DELETE') {
        return mockTagDelete(url);
      } else if (options.method === 'PUT') {
        return mockTagEdit(url, options.body ? JSON.parse(options.body) : {});
      } else {
        return mockTagFetch(url);
      }
    });
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('Renders the Tag Container', () => {
    render(<TagContainer />);
  });

  it('Check for the Input Tag', () => {
    render(<TagContainer />);
    expect(screen.getByRole('heading')).toHaveTextContent(/Input Tag/);
  });

  it('Check for the Add Tag Button and tests it', () => {
    render(<TagContainer />);
    expect(screen.getByText('Add Tag'));
    fireEvent.click(screen.getByText('Add Tag'));
  });

  it('Check for the Edition of Tags and tests the tag edition', async () => {
    const mockTagData = { id: 1, name: 'Metal Tag' };
    const onDeleteTagMock = jest.fn();
    const onEditTagMock = jest.fn();

    render(
      <Tag
        tagId={mockTagData.id}
        tagName={mockTagData.name}
        onDeleteTag={onDeleteTagMock}
        onEditTag={onEditTagMock}
      />
    );

    expect(screen.getByText('x')).toBeInTheDocument();
    fireEvent.click(screen.getByText('x'));
    await mockTagEdit(`/tags/${mockTagData.id}`, { id: mockTagData.id, name: 'New Tag Name' });
  });

  it('Check for the Delete Tag Button and tests tag deletion', async () => {
    const mockTagData = { id: 1, name: 'Metal Tag' };
    const onDeleteTagMock = jest.fn();
    const onEditTagMock = jest.fn();

    render(
      <Tag
        tagId={mockTagData.id}
        tagName={mockTagData.name}
        onDeleteTag={onDeleteTagMock}
        onEditTag={onEditTagMock}
      />
    );

    expect(screen.getByText('x')).toBeInTheDocument();
    fireEvent.click(screen.getByText('x'));
    await mockTagDelete('/tags/1');
  });
});
