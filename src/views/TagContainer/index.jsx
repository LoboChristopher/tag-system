import React, { useContext, useState } from 'react';
import { Container, ContainerSection, ButtonContainer } from './styles';
import { TagsContext } from '@views/Home/context/context';
import Tag from '@components/Tag';
import TextInput from "@components/TextInput"
import { Tagdelete, tagAdd, tagEdit } from '@services/TagService';
import Button from '@components/Button';
import Loading from '@components/Loading/Loading';

export default function TagContainer() {
    const data = useContext(TagsContext);
    const [newTagName, setNewTagName] = useState('');
    const [isEditMode, setIsEditMode] = useState(false);
    const [editTagId, setEditTagId] = useState(null);
    const [editTagName, setEditTagName] = useState('');

    const handleTagDelete = async (tagId) => {
        try {
            await Tagdelete(tagId);
            data.getData();
        } catch (error) {
            console.error('Error deleting tag:', error);
        }
    };

    const handleTagCreate = async () => {
        if (newTagName.trim() === '') {
            return;
        }
        try {
            data.setIsLoading(true);
            await tagAdd(newTagName);
            setNewTagName('');
            data.getData();
        } catch (error) {
            console.error('Error adding tag:', error);
        } finally {
            data.setIsLoading(false);
        }
    }

    const enterEditMode = (tagId, tagName) => {
        setIsEditMode(true);
        setEditTagId(tagId);
        setEditTagName(tagName);
    };

    const handleTagEdit = async () => {
        if (!editTagId || editTagName.trim() === '') {
            return;
        }
        try {
            data.setIsLoading(true);
            await tagEdit(editTagId, editTagName);
            setIsEditMode(false);
            setEditTagId(null);
            setEditTagName('');
            data.getData();
        } catch (error) {
            console.error('Error editing tag:', error);
        } finally {
            data.setIsLoading(false);
        }
    };

    const handleCancelEdit = () => {
        setIsEditMode(false);
        setEditTagId(null);
    }

    return (
        <Container>
            {data?.isLoading ? (
                <Loading color={"#0091ff"} />
            ) : (
                <>
                    <ContainerSection>
                        {data?.data.map(tagData => (
                            <Tag
                                key={tagData.id}
                                tagId={tagData.id}
                                tagName={tagData.name}
                                onDeleteTag={handleTagDelete}
                                onEditTag={enterEditMode}
                            />
                        ))}
                    </ContainerSection>
                    <ContainerSection borderColor={"#fff"} center={"center"} mt={"1rem"}>
                        {isEditMode ? (
                            <>
                                <TextInput
                                    textSize={4}
                                    text={"Edit Tag"}
                                    value={editTagName}
                                    onChange={(e) => setEditTagName(e.target.value)}
                                />
                                <ButtonContainer>
                                    <Button color={"#ab4444"} label={"Cancel Edit"} onClickFunction={handleCancelEdit} />
                                    <Button color={"#4457ab"} label={"Save Edit"} ml={5} onClickFunction={handleTagEdit} />
                                </ButtonContainer>
                            </>
                        ) : (
                            <>
                                <TextInput
                                    textSize={4}
                                    text={"Input Tag"}
                                    value={newTagName}
                                    onChange={(e) => setNewTagName(e.target.value)}
                                />
                                <ButtonContainer>
                                    <Button color={"#4457ab"} label={"Add Tag"} onClickFunction={handleTagCreate} />
                                </ButtonContainer>

                            </>
                        )}
                    </ContainerSection>
                </>
            )}
        </Container>
    );
}
