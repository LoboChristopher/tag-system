import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: #242424;
`;

export const ContainerSection = styled.div`
  width: 40rem;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: ${(props) => (props.borderColor ? "space-between" : "center")};
  flex-wrap: wrap;
  border-style: ${(props) => (props.borderColor ? "solid" : "none")};
  border-width: 2px;
  border-color: ${(props) => props.borderColor};
  border-radius: ${(props) => (props.borderColor ? "4px" : "0")};
  margin-top: ${(props) => props.mt || ""};
  padding: 1rem;
  max-height: 30rem;
  overflow-y: auto;
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: end;
  width: 14rem;
`

