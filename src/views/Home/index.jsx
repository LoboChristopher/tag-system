import React from 'react';
import TagProvider from './context/provider';
import TagContainer from "@views/TagContainer"

export default function Home() {

    return (
        <TagProvider>
            <TagContainer />
        </TagProvider>
    );
}
