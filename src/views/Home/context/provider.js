import React, { useState, useEffect } from "react";
import { TagsContext } from "./context";
import { tagFetch } from "@services/TagService";
import Loading from "@components/Loading/Loading";
import { Container } from "@views/TagContainer/styles";

const TagProvider = ({ children }) => {
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  async function getData() {
    try {
      const apiData = await tagFetch();
      setData(apiData);
      setIsLoading(false);
    } catch (error) {
      alert("There was a problem fetching the tags");
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const contextValue = {
    data,
    isLoading,
    setIsLoading,
    getData,
  };

  if (isLoading) {
    return (
    <Container>
      <Loading color={"#0091ff"} />
    </Container>);
  }

  return (
    <TagsContext.Provider value={contextValue}>
      {children}
    </TagsContext.Provider>
  );
};

export default TagProvider;
