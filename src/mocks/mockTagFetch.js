import api from "@services/api";

const mockTagList = {
    createdAt: "2023-08-18T20:03:05.546Z",
    name: "Black Metal",
    id: "1"
}

export default async function mockTagFetch(url) {

    switch (url) {
        case "https://64dfc52371c3335b25830a34.mockapi.io/tags":
            return {
                ok: true,
                status: 200,
                json: async () => mockTagList,
            };
        default:
            break;
    }
}
