const mockTagEdit = async (url, data) => {
    switch (url) {
      case `https://64dfc52371c3335b25830a34.mockapi.io/tags/${data.id}`:
        return {
          ok: true,
          status: 200,
          json: async () => ({ ...data })
        };
      default:
        break;
    }
  };
  
  export default mockTagEdit;
  