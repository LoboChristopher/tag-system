import Home from "@views/Home"
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <>
      <ToastContainer toastStyle={{ backgroundColor: "#242424", color: "#fff" }}  />
      <Home />
    </>
  );
}

export default App;
