import React from 'react'
import { LoadSpinner } from "./styles"

export default function Loading({ color }) {
    return (
        <LoadSpinner color={color} />
    )
}
