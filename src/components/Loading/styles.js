import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  100% { transform: rotate(0deg); }
  0% { transform: rotate(360deg); }
`;

export const LoadSpinner = styled.div`
  width: 50px;
  height: 50px;
  border: 4px solid rgba(0, 0, 0, 0.1);
  border-top: 6px dotted ${(props) => (props.color ? props.color : '#ab4444')};
  border-radius: 50%;
  animation: ${spin} 1s linear infinite;
`;