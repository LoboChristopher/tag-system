import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  color: #fff;
`;

export const ButtonContainer = styled.div`
  cursor: pointer;
  border-color: ${(props) => props.color || "#fff"};
  border-radius: 4px;
  border-style: solid;
  padding: 1rem;
  transition: background-color 0.3s ease;
  margin-left:  ${(props) => props.ml+"px" || 0};

  &:hover {
    background-color:  ${(props) => props.color || "#fff"};
  }
`