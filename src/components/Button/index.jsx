import React from 'react'
import { Container, ButtonContainer } from './styles'


export default function Button({ label, onClickFunction, color, ml }) {
    return (
        <Container>
            <ButtonContainer color={color} hoverBackgroundColor={color} ml={ml} onClick={onClickFunction}>{label}</ButtonContainer>
        </Container>
    )
}
