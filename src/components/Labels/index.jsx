import React from 'react';
import { Container } from './styles';

export default function Labels({ size, text }) {
    const Heading = `h${size}`;

    return (<Container><Heading>{text}</Heading></Container>);
}