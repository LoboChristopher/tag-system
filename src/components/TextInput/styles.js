import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Input = styled.input`
  color: #fff;
  background-color: #242424;
  padding: 12px 20px;
  margin: 0.5rem 0;
  border-radius: 4px;
  box-sizing: border-box;
`;
