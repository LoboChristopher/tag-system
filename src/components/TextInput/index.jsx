import React from 'react';
import Labels from "@components/Labels";
import { Container, Input } from './styles';

export default function TextInput({ textSize, text, value, onChange }) {
  return (
    <Container>
      <Labels size={textSize} text={text} />
      <Input value={value} onChange={onChange} />
    </Container>
  );
}
