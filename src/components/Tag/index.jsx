import React from 'react';
import { TagButton, TagDeleteButton, TagEditButton } from './styles';

export default function Tag({ tagId, tagName, onDeleteTag, onEditTag }) {
    const handleEditClick = () => {
        onEditTag(tagId, tagName);
    };

    const handleDeleteClick = (e) => {
        e.stopPropagation();
        onDeleteTag(tagId);
    };

    return (
        <TagButton onClick={handleEditClick}>
            <TagEditButton>
                {tagName}
            </TagEditButton>
            <TagDeleteButton onClick={handleDeleteClick}>x</TagDeleteButton>
        </TagButton>
    );
}
