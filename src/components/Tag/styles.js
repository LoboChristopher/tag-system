import React, { useMemo } from "react";
import styled, { css } from "styled-components";

const generateBackgroundColors = () => {
  const min = 150;
  const max = 230;

  const randomComponent = () => Math.floor(Math.random() * (max - min + 1)) + min;

  const r = randomComponent();
  const g = randomComponent();
  const b = randomComponent();

  return `rgb(${r},${g},${b})`;
};

const tagButtonBaseStyles = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1rem;
  color: #fff;
  border-radius: 4px;
  font-size: 16px;
  font-weight: bold;
  width: auto;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  transition: background-color 0.3s ease;
  &:not(:last-child) {
    margin-right: 0.5rem;
  }

  margin-top: 1rem;
  margin-right: 1rem;
`;

const ButtonWrapper = styled.div`
  ${tagButtonBaseStyles};
  cursor: pointer;
  border-color: ${(props) => props.borderPastelColor || props.backgroundColor};
  border-style: solid;
  border-width: 2px;
  border-color: ${(props) => props.backgroundColor};
  &:hover {
    background-color: ${(props) => props.hoverBackgroundColor || props.backgroundColor};
  }
`;

export const TagButton = ({ backgroundColor, hoverBackgroundColor, ...props }) => {
  const generatedBackgroundColor = useMemo(() => generateBackgroundColors(), []);
  return (
    <ButtonWrapper
      backgroundColor={backgroundColor || generatedBackgroundColor}
      hoverBackgroundColor={hoverBackgroundColor}
      {...props}
    />
  );
};

export const TagEditButton = styled.div`
  cursor: pointer;
  margin-right: 1rem;
`;

export const TagDeleteButton = styled.div`
  cursor: pointer;
  color: #fff;
   &:hover {
      color: red;
    }
`;
