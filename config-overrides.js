const path = require('path');

module.exports = {
  webpack: function (config, env) {
    config.resolve.alias = {
      ...config.resolve.alias,
      '@views': path.resolve(__dirname, 'src/views'),
      '@components': path.resolve(__dirname, 'src/components'),
      '@services': path.resolve(__dirname, 'src/services'),
      '@assets': path.resolve(__dirname, 'src/assets'),
      '@context': path.resolve(__dirname, 'src/components/context'),
      '@style': path.resolve(__dirname, 'src/components/style.js'),
      '@mocks': path.resolve(__dirname, 'src/mocks'),
    };
    return config;
  }
};
